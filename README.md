# Vanity URL Kata

- [Vanity URL Kata](#vanity-url-kata)
  - [Requirements](#requirements)
  - [Getting started](#getting-started)
    - [1. Build the docker image](#1-build-the-docker-image)
    - [2. Start the database](#2-start-the-database)
    - [3. Start the rest of the services](#3-start-the-rest-of-the-services)
    - [4. Using the API](#4-using-the-api)
  - [Run Tests](#run-tests)
  - [Shutting down the environment](#shutting-down-the-environment)
  - [Run Locally (ALTERNATIVE, EXPERIMENTAL)](#run-locally-alternative-experimental)
    - [macOS requirements](#macos-requirements)
    - [Starting the rails app](#starting-the-rails-app)
    - [Re-setting the database](#re-setting-the-database)
  - [Other Notes](#other-notes)
    - [Connecting to the (local) database](#connecting-to-the-local-database)
  - [Design Decisions](#design-decisions)
  - [Assumptions](#assumptions)
  - [Opportunities for improvement](#opportunities-for-improvement)

> All script instructions in this section are intended to be run from the project root directory

## Requirements

- Docker Desktop `4.1.1` or later

## Getting started

### 1. Build the docker image

```shell
./scripts/build
```

### 2. Start the database

```shell
./scripts/start_db
```

When the database is ready for connections, the output will look like this:

![MySQL container is ready for connections](images/db_container_is_ready.jpg)

### 3. Start the rest of the services

```shell
./scripts/start_apps
```

### 4. Using the API

- Download [Insomnia](https://insomnia.rest/download)
- Follow [these instructions](https://docs.insomnia.rest/insomnia/import-export-data) to import [this environment](./Insomnia_2021-10-27.yaml)
- Explore the included examples

Alternatively, you can review the `./vanity-url-api/spec/requests/` files to get a sense of how the API endpoints work.


## Run Tests

```shell
# Connect to the running API container
./scripts/connect

# Run the test suite in the container
export RAILS_ENV=test && bundle exec rspec
```

## Shutting down the environment

```shell
docker compose down
```

## Run Locally (ALTERNATIVE, EXPERIMENTAL)

### macOS requirements

- macOS Big Sur or later
- rbenv `1.2.0`
- ruby `2.7.1`
- rails `6.1.4`
- mysql2 gem ([instructions here](https://github.com/brianmario/mysql2))

### Starting the rails app

```shell
# CD to the API directory
cd <project-root>/vanity-url-api

# (Re-)setup the database
./scripts/setup

# Start the server
./scripts/server
```

### Re-setting the database

```shell
# CD to the API directory
cd <project-root>vanity-url-api


./scripts/reset
```

## Other Notes

### Connecting to the (local) database

Server: `mysql.vanity.local`
Username: `root`
Password: `Test1234`
Port: `3306`

## Design Decisions

- With the option of the following alpha numeric characters `abcdefghijklmnopqrstuvwxyz123456789`, the service would need to generate a minimum of 4 characters to support `1,256,640` unique short codes.
- Would research `ActionCable` before deployment to a production environment to determine if that feature of Rails is needed for the API (suspect no, but might be wrong). Skipping to stay focused on the required features of the exercise.
- Considered using `RSwag` for testing concerns but decided on the simplicity of request specs - informed by the simplicity of the solution (relative to one that had authentication/authorization etc. concerns implemented and could benefit from the robust features of `RSwag` for REST API documentation & contract assertions).

## Assumptions

- Only successful `GET /link/:short_code` requests are interesting. No tracking of "clicks" with invalid short codes is implemented. An idea for implementing that: *a migration to add a `:redirected` boolean attribute to the `Click` model, with a default of `1` that can be set to `0` for clicks that do not resolve to a valid `Link`*

## Opportunities for improvement

- I wondered if the `ApplicationController` knew too much about the `LinkController` and `ClickController` use cases. This trade off was acceptable for the exercise but for a larger, production application, the `ApplicationController` should only concern itself with widely applicable functionality for the API (e.g. error the error rescue methods).
