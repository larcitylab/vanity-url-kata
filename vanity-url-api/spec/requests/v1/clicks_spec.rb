require 'fixture_helper'

RSpec.describe "Clicks", type: :request do
  include FixtureHelper

  before(:all) do
    Rails.application.load_seed
  end

  let(:body_hash) { JSON.parse(response.body).map(&:with_indifferent_access) }

  describe "POST /v1/links/clicks" do
    before(:each) do
      post "/v1/links/clicks"
    end

    it 'returns the expected data size' do
      expect(response.status).to be(200)
      expect(body_hash.size).to eq(1011)
    end

    it 'reports on the expected attributes' do
      first_agg = body_hash.first
      expect(first_agg).to \
        match(
          hash_including(
            :id,
            :short_code,
            :session_user_id,
            :last_visited,
            :impressions,
          ),
        )
    end
  end

  describe "POST /v1/links/:short_code/clicks" do
    let(:short_code) { 'hg9i' }

    before(:each) do
      post "/v1/links/#{short_code}/clicks"
    end

    it 'return the expected data (per short code)' do
      expect(response.status).to eq(200)
      expect(body_hash.size).to eq(13)
    end

    it 'reports on the expected attributes (per short code)' do
      first_agg = body_hash.first
      expect(first_agg).to \
        match(
          hash_including(
            :id,
            :short_code,
            :session_user_id,
            :last_visited,
            :impressions,
          ),
        )
    end
  end
end
