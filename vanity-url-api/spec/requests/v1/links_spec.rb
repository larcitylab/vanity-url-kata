RSpec.describe "Links", type: :request do
  let(:mock_link_url) { 'https://bing.com' }

  describe "GET /v1/links/:short_code" do
    let!(:link) { Link.create(destination_url: mock_link_url) }
    let(:short_code) { link.short_code }

    it 'redirects to the destination URL for an existing link' do
      get "/v1/links/#{short_code}"
      expect(response.status).to be(302)
      expect(response.header['Location']).to eq(mock_link_url)
    end
  end

  describe "POST /v1/links" do
    let(:body_hash) { JSON.parse(response.body).with_indifferent_access }

    before(:each) do
      post "/v1/links", params: { destination_url: mock_link_url }
    end

    it 'creates a new link with valid params' do
      expect(response.status).to be(200)
      expect(body_hash[:destination_url]).to eq(mock_link_url)
    end

    it 'returns a short code in the response body' do
      expect(body_hash[:short_code]).not_to be_nil
    end

    context 'with invalid params' do
      let(:mock_link_url) { nil }

      it 'returns the expected error response' do
        expect(response.status).to eq(400)
      end

      it 'returns a useful error message' do
        expect(body_hash[:destination_url]).to eq(["can't be blank", "is not a valid URL"])
      end
    end
  end
end
