RSpec.describe "Defaults", type: :request do
  describe "GET /index" do
    it 'returns "not implemented" HTTP status' do
      get ''
      expect(response.status).to eq(501)
    end
  end
end
