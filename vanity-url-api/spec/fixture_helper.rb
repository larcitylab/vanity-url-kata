require 'yaml'

module FixtureHelper
  def fixture(rel_path, type: :yaml)
    return YAML.load_file("spec/fixtures/#{rel_path}") if type == :yaml

    nil
  end
end
