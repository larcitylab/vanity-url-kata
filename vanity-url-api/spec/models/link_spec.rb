require 'validate_url/rspec_matcher'

describe Link do
  let(:attributes) { {} }
  subject { described_class.new(attributes) }

  it { is_expected.to validate_url_of(:destination_url) }

  it 'validates URLs' do
    link = described_class.new(destination_url: 'something invalid')
    expect(link.valid?).to be(false)
  end

  it 'is able to access the configuration for the retry limit' do
    expect(Rails.configuration.short_code[:uniqueness_retry_limit]).to be_between(3, 5)
  end

  it 'creates a uuid after initialization' do
    expect(subject.id).not_to be_nil
  end

  it 'creates a short code after initialization' do
    link = subject
    expect(link.short_code).not_to be_nil
  end

  it 'does not create a new link without destination URL' do
    expect(subject.save).to be(false)
  end

  context ':unsaved?' do
    let(:attributes) { { destination_url: 'https://bing.com'} }
    let(:saved_link) { described_class.create(attributes) }

    it 'returns true for new instances' do
      expect(subject.unsaved?).to be(true)
    end

    it 'returns false once saved' do
      new_link = subject
      new_link.save
      expect(new_link.unsaved?).to be(false)
    end

    it 'returns false for retrieved links' do
      expect(saved_link.unsaved?).to be(false)
    end
  end

  context 'with destination URL' do
    let(:attributes) { { destination_url: 'https://bing.com' } }

    it 'creates a link successfully' do
      expect(subject.save).to be(true)
    end
  end
end
