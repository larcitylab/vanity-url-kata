# Vanity URL API

> All script instructions in this section are intended to be run from the rails app 
> root directory: `<project-root>/vanity-url-api`

## Creating the rails app

```shell
rails new vanity-url-api2 --api --minimal --dev --database=mysql --skip-action-mailer --skip-action-mailbox --skip-active-storage --skip-javascript --skip-turbolinks --skip-test
```

## Seeding data (will not work in production)

```shell
bin/rails db:seed
```

## Clearing all the data in the database

```shell
bin/rake links:reset_data 
```

## Running the test suite

```shell
bundle exec rspec
```
