class ValidationError < StandardError
  attr_accessor :model

  def initialize(msg = nil, model:)
    @model = model if model.present?
    super(msg)
  end
end
