class DefaultController < V1::ApplicationController
  def show
    render json: { service: 'Vanity URL API', version: 1 }, status: :not_implemented
  end
end
