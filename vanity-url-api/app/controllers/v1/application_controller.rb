class V1::ApplicationController < ActionController::API
  rescue_from ResourceNotFoundError, with: :resource_not_found
  rescue_from InvalidResourceError, with: :invalid_resource_error
  rescue_from ValidationError, with: :validation_error

  attr_reader :link

  before_action :set_session_id

  protected

  def get_link_params
    params.permit(:short_code)
  end

  def set_link
    @link = Link.find_by(short_code: get_link_params[:short_code])
    raise ResourceNotFoundError, 'Link not found' unless link.present?
  end

  def short_code
    link.short_code
  end

  private

  def set_session_id
    session[:user_id] ||= SecureRandom.uuid
  end

  def validation_error(exception)
    render json: exception.model.errors, status: :bad_request
  end

  def resource_not_found(exception)
    render json: { message: exception.to_s }, status: :not_found
  end

  def invalid_resource_error(exception)
    render json: { message: exception.to_s }, status: :unprocessable_entity
  end
end
