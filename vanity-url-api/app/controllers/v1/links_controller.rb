module V1
  class LinksController < ApplicationController
    before_action :set_link, only: [:show]

    def create
      new_link = Link.new(create_link_params)

      raise ValidationError.new('Validation failed', model: new_link) unless new_link.valid?

      new_link.save!
      render json: new_link.serializable_hash
    end

    def show
      raise InvalidResourceError, 'Invalid link' unless link.destination_url.present?

      Click.create(short_code: short_code, session_user_id: session[:user_id], user_agent: request.user_agent)

      redirect_to link.destination_url
    end

    private

    def create_link_params
      params.permit(:destination_url)
    end
  end
end

