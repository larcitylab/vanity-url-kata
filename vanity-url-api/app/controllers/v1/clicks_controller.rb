module V1
  class ClicksController < ApplicationController
    before_action :set_link, only: [:show]

    def show
      render json: { message: 'short_code is required' }, status: :bad_request \
        unless short_code.present?

      render_report(link.impressions_report)
    end

    def show_all
      render_report(Link.all_impressions_report)
    end

    private

    def render_report(clicks)
      render json: clicks.map { |click| click.serializable_hash }
    end
  end
end
