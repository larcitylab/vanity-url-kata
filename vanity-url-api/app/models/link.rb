class Link < ApplicationRecord
  class << self
    def all_impressions_report
      query = <<~SQL
        select
        links.id,
        clicks.short_code
        , session_user_id
        , max(clicks.created_at) as last_visited
        , count(session_user_id) as impressions
        from clicks
        left join links
        on links.short_code = clicks.short_code
        group by 1, 2, 3
        order by impressions desc
      SQL

      find_by_sql(query)
    end
  end

  def impressions_report
    query = <<~SQL
      select
      links.id,
      clicks.short_code
      , session_user_id
      , max(clicks.created_at) as last_visited
      , count(session_user_id) as impressions
      from clicks
      left join links
      on links.short_code = clicks.short_code
      where clicks.short_code = ?
      group by session_user_id
      order by impressions desc
    SQL

    self.class.find_by_sql([query, short_code])
  end

  validates :short_code, presence: true, uniqueness: true
  validates :destination_url, presence: true, url: true

  after_initialize :generate_short_code

  private

  def generate_short_code
    if self.short_code.blank?
      # TODO: (Rails) server configurations can afford more options here with a base58 configuration
      #   if the server is configured to support case sensitive URLs
      new_code = SecureRandom.base36(4)
      attempts = 1
      while Link.find_by(short_code: new_code).present? do
        # TODO: test graceful handling of this error with a request spec to ensure the right HTTP response
        #   is sent to the client (server side 500 with helpful message)
        raise 'Failed to generate a unique short code' \
          if attempts > Rails.configuration.short_code[:uniqueness_retry_limit]

        new_code = SecureRandom.base36(4)
        attempts += 1
      end
      self.short_code = new_code
    end
  end
end
