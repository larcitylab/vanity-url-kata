class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  after_initialize :generate_uuid

  def unsaved?
    self.created_at.blank?
  end

  def uses_uuid?
    self.class.columns_hash['id'].type == :string
  end

  def generate_uuid
    self.id ||= SecureRandom.uuid if uses_uuid?
  end
end
