# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
#
require 'yaml'

def seed_fixture(rel_path)
  YAML.load_file("spec/fixtures/#{rel_path}")
end

if Rails.env.production?
  puts 'Skipping seeding data in production...'
elsif Rails.env.test?
  Link.destroy_all
  Click.destroy_all

  links = seed_fixture('links.yml')
  puts "Links loaded: #{links.size}..."
  Link.create(links)

  clicks = seed_fixture('clicks.yml')
  puts "Clicks loaded: #{clicks.size}..."
  Click.create(clicks)
else
  Link.destroy_all
  Click.destroy_all

  user_agent_data = [
    {
      browser: :chrome,
      string: 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36'
    },
    {
      browser: :ms_edge,
      string: 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36 Edg/91.0.864.59',
    },
    {
      browser: :ie,
      string: 'Mozilla/5.0 (compatible; MSIE 9.0; Windows Phone OS 7.5; Trident/5.0; IEMobile/9.0)'
    },
    {
      browser: :safari,
      string: 'Mozilla/5.0 (iPhone; CPU iPhone OS 13_5_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.1 Mobile/15E148 Safari/604.1'
    }
  ].map { |data| OpenStruct.new(data) }
  link_data = [
    {
      destination_url: "https://en.wikipedia.org/wiki/Kama_Sutra"
    },
    {
      destination_url: "https://en.wikipedia.org/wiki/History_of_India"
    },
    {
      destination_url: "https://en.wikipedia.org/wiki/Inferno_(Dante)"
    },
    {
      destination_url: "https://en.wikipedia.org/wiki/Italy_in_the_Middle_Ages"
    },
    {
      destination_url: "https://en.wikipedia.org/wiki/Bible"
    },
    {
      destination_url: "https://en.wikipedia.org/wiki/History_of_ancient_Israel_and_Judah"
    },
    {
      destination_url: "https://en.wikipedia.org/wiki/Odyssey"
    },
    {
      destination_url: "https://en.wikipedia.org/wiki/Ancient_Greece"
    },
    {
      destination_url: "https://en.wikipedia.org/wiki/Beowulf"
    },
    {
      destination_url: "https://en.wikipedia.org/wiki/England_in_the_Middle_Ages"
    },
    {
      destination_url: "https://en.wikipedia.org/wiki/Quran"
    },
    {
      destination_url: "https://en.wikipedia.org/wiki/Caliphate"
    },
    {
      destination_url: "https://en.wikipedia.org/wiki/Rosetta_Stone"
    },
    {
      destination_url: "https://en.wikipedia.org/wiki/Ancient_Egypt"
    },
    {
      destination_url: "https://en.wikipedia.org/wiki/Mahabharata"
    },
    {
      destination_url: "https://en.wikipedia.org/wiki/History_of_India"
    },
    {
      destination_url: "https://en.wikipedia.org/wiki/Divine_Comedy"
    },
    {
      destination_url: "https://en.wikipedia.org/wiki/Italy_in_the_Middle_Ages"
    },
    {
      destination_url: "https://en.wikipedia.org/wiki/Iliad"
    },
    {
      destination_url: "https://en.wikipedia.org/wiki/Ancient_Greece"
    },
    {
      destination_url: "https://en.wikipedia.org/wiki/Ramayana"
    },
    {
      destination_url: "https://en.wikipedia.org/wiki/History_of_India"
    },
    {
      destination_url: "https://en.wikipedia.org/wiki/The_Art_of_War"
    },
    {
      destination_url: "https://en.wikipedia.org/wiki/History_of_China"
    },
    {
      destination_url: "https://en.wikipedia.org/wiki/Epic_of_Gilgamesh"
    },
    {
      destination_url: "https://en.wikipedia.org/wiki/Mesopotamia"
    },
    {
      destination_url: "https://en.wikipedia.org/wiki/Vedas"
    },
    {
      destination_url: "https://en.wikipedia.org/wiki/History_of_India"
    },
    {
      destination_url: "https://en.wikipedia.org/wiki/The_Canterbury_Tales"
    },
    {
      destination_url: "https://en.wikipedia.org/wiki/England_in_the_Middle_Ages"
    },
    {
      destination_url: "https://en.wikipedia.org/wiki/One_Thousand_and_One_Nights"
    },
    {
      destination_url: "https://en.wikipedia.org/wiki/Caliphate"
    },
    {
      destination_url: "https://en.wikipedia.org/wiki/Talmud"
    },
    {
      destination_url: "https://en.wikipedia.org/wiki/Judaism"
    },
    {
      destination_url: "https://en.wikipedia.org/wiki/Code_of_Hammurabi"
    },
    {
      destination_url: "https://en.wikipedia.org/wiki/Mesopotamia"
    },
    {
      destination_url: "https://en.wikipedia.org/wiki/Oedipus_the_King"
    },
    {
      destination_url: "https://en.wikipedia.org/wiki/Ancient_Greece"
    },
    {
      destination_url: "https://en.wikipedia.org/wiki/I_Ching"
    },
    {
      destination_url: "https://en.wikipedia.org/wiki/History_of_China"
    },
    {
      destination_url: "https://en.wikipedia.org/wiki/Aeneid"
    },
    {
      destination_url: "https://en.wikipedia.org/wiki/Ancient_Rome"
    },
    {
      destination_url: "https://en.wikipedia.org/wiki/Book_of_the_Dead"
    },
    {
      destination_url: "https://en.wikipedia.org/wiki/Ancient_Egypt"
    },
    {
      destination_url: "https://en.wikipedia.org/wiki/Rigveda"
    },
    {
      destination_url: "https://en.wikipedia.org/wiki/History_of_India"
    },
    {
      destination_url: "https://en.wikipedia.org/wiki/Domesday_Book"
    },
    {
      destination_url: "https://en.wikipedia.org/wiki/England_in_the_Middle_Ages"
    },
    {
      destination_url: "https://en.wikipedia.org/wiki/Tao_Te_Ching"
    },
    {
      destination_url: "https://en.wikipedia.org/wiki/History_of_China"
    },
    {
      destination_url: "https://en.wikipedia.org/wiki/Upanishads"
    },
    {
      destination_url: "https://en.wikipedia.org/wiki/History_of_India"
    },
    {
      destination_url: "https://en.wikipedia.org/wiki/Cupid_and_Psyche"
    },
    {
      destination_url: "https://en.wikipedia.org/wiki/Ancient_Greece"
    },
    {
      destination_url: "https://en.wikipedia.org/wiki/Lysistrata"
    },
    {
      destination_url: "https://en.wikipedia.org/wiki/Ancient_Greece"
    },
    {
      destination_url: "https://en.wikipedia.org/wiki/The_Decameron"
    },
    {
      destination_url: "https://en.wikipedia.org/wiki/Italy_in_the_Middle_Ages"
    },
    {
      destination_url: "https://en.wikipedia.org/wiki/Titanomachy"
    },
    {
      destination_url: "https://en.wikipedia.org/wiki/Ancient_Greece"
    }
  ]

  puts 'Building link data...'

  puts "Seeded #{link_data.size} link(s)" if Link.create!(link_data)

  seed_links = Link.all

  puts 'Building hits data...'

  # Seeding unique hits
  unique_hits_data = 100.times.map do
    selected_link = seed_links[rand(0...link_data.size)]
    selected_agent = user_agent_data[rand(0...user_agent_data.size)]

    {
      short_code: selected_link.short_code,
      session_user_id: SecureRandom.uuid,
      user_agent: selected_agent.string,
    }
  end

  puts "Seeded #{unique_hits_data.size} unique hit(s)" if Click.create!(unique_hits_data)

  # Seeding returning user hits
  user_sessions = 10.times.map { OpenStruct.new(user_id: SecureRandom.uuid) }
  high_traffic_links = seed_links[0..10]
  high_traffic_links.each do |link|
    sess = user_sessions[rand(0...user_sessions.size)]
    selected_agent = user_agent_data[rand(0...user_agent_data.size)]
    traffic = 25.times.map do
      {
        short_code: link.short_code,
        session_user_id: sess.user_id,
        user_agent: selected_agent.string,
      }
    end
    puts "Created #{traffic.size} known user hits for link with short code #{link.short_code}" \
      if Click.create!(traffic)
  end
end
