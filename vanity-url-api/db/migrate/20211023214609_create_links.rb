class CreateLinks < ActiveRecord::Migration[6.1]
  def up
    create_table :links, id: false do |t|
      t.primary_key :id, :string, limit: 50
      t.string :short_code, null: false
      t.string :destination_url, null: false
      t.boolean :active, default: true

      t.timestamps
    end
    add_index :links, :short_code, unique: true, name: 'unique_short_code'
  end

  def down
    remove_index :links, name: 'unique_short_code'
    drop_table :links
  end
end
