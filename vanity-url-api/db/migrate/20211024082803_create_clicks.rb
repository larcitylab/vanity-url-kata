class CreateClicks < ActiveRecord::Migration[6.1]
  def up
    create_table :clicks, id: false do |t|
      t.primary_key :id, :string, limit: 50
      t.string :session_user_id, limit: 50, null: false
      t.string :short_code, limit: 10, null: false
      t.text :user_agent, limit: 1000

      t.timestamps
    end
    add_index :clicks, [:session_user_id, :short_code], if_not_exists: true
  end

  def down

  end
end
