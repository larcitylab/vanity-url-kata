namespace :links do
  desc 'Reset data'
  task reset_data: :environment do
    raise StandardError.new('This rake task will not made data changes in production!') \
      if Rails.env.production?

    puts "Starting truncation of #{Rails.env} data..."

    Click.destroy_all
    Link.destroy_all

    puts 'Done!'
  end
end
