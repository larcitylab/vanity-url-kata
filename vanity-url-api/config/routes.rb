Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  namespace :v1, format: :json do
    resources :links, only: [:create] do
      collection do
        post 'clicks', controller: :clicks, action: :show_all
      end
    end

    get 'links/:short_code', controller: :links, action: :show
    # TODO: intentionally implementing with POST to bias this resource
    #   for integration access (vs. simple browser requests to GET the data)
    #
    # TODO: Before deployment to production environment, would address
    #   authorization concerns for this (and other) resource(s). See README
    #   for details
    #
    # TODO Authorization high level ideas:
    #   certificate credentials, oauth application credentials,
    #   platform-specific endpoint security e.g. using GCP / AWS API Gateway
    #   features for securing endpoints within networks etc.
    post 'links/:short_code/clicks', controller: :clicks, action: :show
  end

  root controller: :default, action: :show
end
